package fr.uvsq.tod;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

/**
 * MessageController représente les différentes actions possibles sur l'application et 
 * contient les requêtes qui permettent à l'utilisateur d'interagir.
 * 
 * @author Romain
 * 
 * @version 2.0
 * 
 * 18/11/2017
 * 
 * */
@RestController
 public class MessageController {
	/**
	 * tasks représente la liste des tâches créées pour l'application. Ne peut être modifié que par la classe.
	 * User représente la liste des utilisateurs créées pour l'application.
	 * NoUser représente une liste vide, renvoyer dans la fonction ListUser elle permet de ne rien afficher.
	 * current_user est l'utilisateur courant de lapplication.
	 */
 	private static ArrayList<Task> tasks = new ArrayList<Task>();
 	private static ArrayList<Utilisateur> User = new ArrayList<Utilisateur>();
 	private static ArrayList<Utilisateur> NoUser = new ArrayList<Utilisateur>();
 	private Utilisateur current_user = new Utilisateur("", "", "");

 	/**
 	 * Créer deux tâches de base et les ajoute à la liste des tâches, si la liste de tâches est vide.
 	 */
	private static void init()
	{
	}
	/**
     * Permet de d'ajouter un utilisateur dans la liste.
     * @param nom
     * 				nom de l'utilisateur.
     * @param prenom
     * 				prenom de l'utilisateur.
     * @param mdp
     * 				mot de passe de l'utilisateur
     * @return un message en cas de réussite (nom et prenom) ou échec en cas de doublon dans la liste.
     */
	@RequestMapping("/addUser")
	public String addUser(@RequestParam String nom, @RequestParam String prenom, @RequestParam String mdp) {
		for(Utilisateur c : User)
        {
			if(c.getNom().equals(nom) && c.getPrenom().equals(prenom))
				return "le compte existe déjà ...";
		}
        User.add(new Utilisateur(nom,prenom,mdp));
        return nom + " " + prenom;
    }
    
    /**
     * Permet de connecter l'utilisateur.
     * @param nom
     * 				nom de l'utilisateur.
     * @param mdp
     * 				mot de passe de l'utilisateur
     * @return un message en cas de réussite ("Welcome" + user) ou échec en cas mauvais mot de passe ou nom.
     */
    @RequestMapping("/login")
	public String login(@RequestParam String nom, @RequestParam String mdp) {
		if(current_user.getNom() == nom){
			return "Vous êtes déjà connecté : " + nom + ".";
		}
        for(Utilisateur c : User)
        {
			if(c.getNom().equals(nom) && c.getMdp().equals(mdp))
			{
				current_user.changer(c);
				return "Welcome " + current_user.getNom() + " !";
			}
		}
        return "Wrong password !";
    }

    /**
     * Permet d'afficher la liste des utilisateur.
     * 
     * @return la liste des utilisateurs si le crédit de l'utilisateur courant est de 5 ou plus, sinon renvois une liste vide.
     */
	@RequestMapping("/listUser")
	public ArrayList<Utilisateur> afficherList() {
        if(current_user.testCredit())
			return User;
		else
			return NoUser;
    }
	
    /**
     * Permet de déconnecter l'utilisateur.
     * 
     * @return un message en cas de réussite ("Vous êtes déconnecté.") ou un message d'erreur si l'utilisateur courant est vide.
     */
	@RequestMapping("/logout")
	public String logout() {
		if(current_user.getNom().equals("")){
			return "Vous n'êtes actuellement pas connecté";
		}
		current_user.effacer();
		return "Vous êtes déconnecté.";
	}
	
    /**
     * Permet de connecter l'utilisateur.
     * @param credit
     * 				le nombre de credit qui va être conféré à l'utilisateur courant.
     * @return un message en cas de réussite ("You are powerfull") ou un message d'erreur si la valeur entré n'était pas attendu (if).
     */
	@RequestMapping("/give")
	public String give(@RequestParam(value="credit", required=true) int credit) {
		if(credit < 0 || credit > 5){
			return "Vous devez mettre une valeur comprise entre 0 et 5";
		}
		current_user.switchCredit(credit);
		for(Utilisateur c : User)
        {
			if(c.getNom().equals(current_user.getNom())){
				c.switchCredit(credit);
			}
		}
		return "You are powerfull";
	}


/**
 * Utilise la méthode @see init pour s'assurer que la liste des tâches n'est pas vide, puis renvoit la liste des tâches gràce à la requête GET.
 * 
 * @return la liste des tâches actuelles de l'application.
 */
    @RequestMapping( path="/",method = RequestMethod.GET)
    public ArrayList<Task> index() {
    	init();
        return tasks;
    }
/**
 * Permet d'ajouter une tâche à la liste des tâches.
 * @param ajout
 * 				le nom de l'objet à ajouter, si aucun nom est indiqué, prend la valeur "nothing".
 * @param model
 * 				WIP
 * @return la liste des tâches avec l'élément ajouté.
 */

    @RequestMapping("/ajout")
    public ArrayList<Task> ajout(@RequestParam(value="ajout", required=false, defaultValue="nothing") String ajout, Model model) {
        model.addAttribute("ajout", ajout);
        tasks.add(new Task("tâche : " + ajout + "  de " + current_user.getNom() + " " + current_user.getPrenom()));
        return tasks;
    }
    /**
     * Vide complètement la liste des tâches.
     * @return la liste des tâches, vide.
     */
    @RequestMapping("/clear")
    public ArrayList<Task> clear() {
        tasks.clear();
        return tasks;
    }
    /**
     * Permet de retirer un objet de la liste en utilisant son identifiant.
     * @param delete 
     * 				L'identifiant de l'objet à supprimer.
     * @param model
     * 				WIP
     * @return la nouvelle liste des tâches avec l'élément supprimé.
     */
    @RequestMapping("/delete")
    public ArrayList<Task> delete(@RequestParam(value="delete", required=false, defaultValue="-1") int delete, Model model) {
        model.addAttribute("delete", delete);
        tasks.remove(delete);
        return tasks;
    }
/**
 * Permet à l'utilisateur de choisir un nom de tâche à ajouter via la requête POST.
 * @param title 
 * 				le nom de la tâche à ajouter.
 */
	@RequestMapping( path="/",method = RequestMethod.POST)
     public void ajout(@RequestBody String title) {
     	tasks.add(new Task(title));
	}
	/**
	 * Récupère toute entrée non prévue et affiche un message d'erreur sur la page.
	 * @return le message d'erreur "fallback method"
	 */
	@RequestMapping("*")
	public String fallbackMethod(){
	return "fallback method";
	}
}
