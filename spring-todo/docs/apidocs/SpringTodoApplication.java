package fr.uvsq.tod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
/**
 * SpringTodoApplication possède le main() du programme et permet d'envoyer une requête à l'application
 * 
 * @author Romain
 *
 * @version 1.0
 * 
 * 18/11/2017
 * 
 * */
@SpringBootApplication
public class SpringTodoApplication {
	/**
	 * Permet à l'utilisateur d'interagir avec l'application
	 * 
	 * @param args
	 * 				la requête envoyée à l'application
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringTodoApplication.class, args);
	}
}
