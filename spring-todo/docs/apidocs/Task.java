/**
 * Task représente une tâche que l'ont peut créer et manipuler à l'aide des requêtes.
 * 
 * @author Romain
 * @version 1.0
 * 
 * 18/11/2017
 * */

package fr.uvsq.tod;

public class Task {

	/**
	 * title représente le nom de la tache.
	 */
    private final String title;
    
    /**
     * Constructeur pour une instance de "Task".
     * @param nom de la tâche
     */
    public Task(String title) {
        this.title = title;
    }
    /**
     * "getter" pour récupérer le nom de la tâche courante.
     * @return le nom de la tâche
     */
    public String getTitle() {
        return this.title;
    }
}