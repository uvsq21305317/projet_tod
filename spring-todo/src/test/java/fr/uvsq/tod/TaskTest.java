package fr.uvsq.tod;

import junit.framework.TestCase;
import org.junit.*;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
public class TaskTest{

	private Task tache = new Task("title");
	
    @Test
    public void testGetTitle(){
      assertFalse("La Tâche n'a pas de nom.", tache.getTitle().equals(""));
    }
}
