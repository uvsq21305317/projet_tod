package fr.uvsq.tod;

import junit.framework.TestCase;
import org.junit.*;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
public class UtilisateurTest{
	protected Utilisateur user = new Utilisateur("Nom","Prenom","Mdp");

  @Test
  public void testUtilisateur(){
    assertNotNull("l'utilisateur n'a pas été créée.",user);
  }
  @Test
  public void testGetNom(){
    assertFalse("le nom ne peut pas être vide", user.getNom().equals(""));
  }
  @Test
  public void testGetPrenom(){
    assertFalse("le prenom ne peut pas être vide", user.getPrenom().equals(""));
  }
  @Test
  public void testGetMdp(){
    assertFalse("le mot de passe ne peut pas être vide", user.getMdp().equals(""));
  }
  @Test
  public void testGetCredit(){
    assertFalse("le crédit n'est pas valable.", user.getCredit() > 5 || user.getCredit() < 0);
  }
  @Test
  public void testSwitchCredit(){
    assertFalse("le nom ne peut pas être vide", user.getCredit() > 5 || user.getCredit() < 0);
  }
  @Test
  public void testChanger(){
	  Utilisateur user1 = new Utilisateur("Nom1", "Prenom1", "Mdp1");
    user.changer(user1);
    assertTrue("L'utilisateur n'a pas été entièrement changé",user.getNom().equals("Nom1") && user.getPrenom().equals("Prenom1") && user.getMdp().equals("Mdp1"));
  }


  @Test
  public void testEffacer(){
    user.effacer();
    assertTrue("L'utilisateur n'a pas été entièrement supprimé",user.getNom().equals("") && user.getPrenom().equals("") && user.getMdp().equals("") && user.getCredit() == 0);
  }



}
