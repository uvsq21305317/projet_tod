/**
 * Utilisateur représente une personne que l'ont peut créer et manipuler à l'aide des requêtes.
 *
 * @author Romain
 * @version 1.0
 *
 * 20/11/2017
 * */
package fr.uvsq.tod;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Utilisateur {

	/**
	 * Id est un paramètre de l'utilisateur, il permet de l'identifier, il est construit automatiquement (GeneratedValue(...)).
	 */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    /**
	 * prenom représente le prénom de l'utilisateur.
	 */
    private String prenom;
    /**
	 * nom représente le nom de l'utilisateur.
	 */
    private String nom;
    /**
	 * mdp représente le mot de passe de l'utilisateur.
	 */
    private String mdp;
    /**
	 * credit represente le pouvoir de l'utilisateur.
	 */
    private int credit;

	/**
     * Constructeur pour une instance de "Utilisateur".
     * @param nom
     * 				nom de la personne
     * @param prenom
     * 				prénom de la personne
     * @param mdp
     * 				mot de passe de la personne
     */
    public Utilisateur(String nom, String prenom, String mdp) {
        this.prenom = prenom;
        this.nom = nom;
        this.mdp = mdp;
        this. credit =0;
    }

	/**
     * Fonction permettant de transformer l'utilisateur en un string.
     * @return Utilisateur sous format de String.
     */
    @Override
    public String toString() {
        return String.format("Utilisateur[id=%d, prenom='%s', nom='%s']", id, prenom, nom);
    }
    /**
     * test le crédit de l'utilisateur (credit >4 signifie avoir le pouvoir d'administrateur).
     * @return vrai si credit est supérieur à 4 sinon faux.
     */
    public boolean testCredit() {
		if(this.credit > 4)
			return true;
		else
			return false;
	}

	/**
     * "getter" pour récupérer le nom de l'utilisateur courant.
     * @return le nom de l'utilisateur
     */
    public String getNom() {
		return this.nom;
	}

	/**
     * "getter" pour récupérer le prénom de l'utilisateur courant.
     * @return le prenom de l'utilisateur
     */
	public String getPrenom(){
		return this.prenom;
	}

	/**
     * "getter" pour récupérer le mot de passe de l'utilisateur courant.
     * @return le mdp de l'utilisateur
     */
	public String getMdp(){
		return this.mdp;
	}

	/**
     * "getter" pour récupérer le crédit de l'utilisateur courant.
     * @return le credit de l'utilisateur
     */
	public int getCredit() {
		return this.credit;
	}

	/**
     * Changer le crédit de l'utilisateur courant.
     *
     * @param newCredit
     * 					nouveau crédit de l'utilisateur
     */
	public void switchCredit(int newCredit) {
		this.credit = newCredit;
	}
	/**
     * Fonction pour effacer l'utilisateur (utilisé pour éffacer les données dans current_user).
     */
	public void effacer() {
		this.prenom = "";
		this.nom = "";
		this.mdp = "";
		this.credit = 0;
	}

	/**
     * fonction pour changer d'utilisateur
     *
     * @param user
     * 					l'objet courant prends les champs de user
     */
	public void changer(Utilisateur user) {
		this.prenom = user.prenom;
		this.nom = user.nom;
		this.mdp = user.mdp;
		this.credit = user.credit;
	}
}
