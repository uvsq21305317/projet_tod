package fr.uvsq.tod;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

/**
 * MessageController represente les differentes actions possibles sur l'application et 
 * contient les requetes qui permettent a l'utilisateur d'interagir.
 * 
 * @author Romain
 * 
 * @version 2.0
 * 
 * 18/11/2017
 * 
 * */
@RestController
 public class MessageController {
	/**
	 * tasks represente la liste des taches creees pour l'application. Ne peut etre modifie que par la classe.
	 * User represente la liste des utilisateurs creees pour l'application.
	 * NoUser represente une liste vide, renvoyee dans la fonction ListUser elle permet de ne rien afficher.
	 * current_user est l'utilisateur courant de l'application.
	 */
 	private static ArrayList<Task> tasks = new ArrayList<Task>();
 	private static ArrayList<Utilisateur> User = new ArrayList<Utilisateur>();
 	private static ArrayList<Utilisateur> NoUser = new ArrayList<Utilisateur>();
 	private Utilisateur current_user = new Utilisateur("", "", "");

 	/**
 	 * Creer deux taches de base et les ajoute a la liste des taches, si la liste de taches est vide.
 	 */
	private static void init()
	{
	}
	/**
     * Permet de d'ajouter un utilisateur dans la liste.
     * @param nom
     * 				nom de l'utilisateur.
     * @param prenom
     * 				prenom de l'utilisateur.
     * @param mdp
     * 				mot de passe de l'utilisateur
     * @return un message en cas de reussite (nom et prenom) ou echec en cas de doublon dans la liste.
     */
	@RequestMapping("/addUser")
	public String addUser(@RequestParam String nom, @RequestParam String prenom, @RequestParam String mdp) {
		for(Utilisateur c : User)
        {
			if(c.getNom().equals(nom) && c.getPrenom().equals(prenom))
				return "le compte existe deja ...";
		}
        User.add(new Utilisateur(nom,prenom,mdp));
        return nom + " " + prenom;
    }
    
    /**
     * Permet de connecter l'utilisateur.
     * @param nom
     * 				nom de l'utilisateur.
     * @param mdp
     * 				mot de passe de l'utilisateur
     * @return un message en cas de reussite ("Welcome" + user) ou echec en cas mauvais mot de passe ou nom.
     */
    @RequestMapping("/login")
	public String login(@RequestParam String nom, @RequestParam String mdp) {
		if(current_user.getNom() == nom){
			return "Vous etes deja connecte : " + nom + ".";
		}
        for(Utilisateur c : User)
        {
			if(c.getNom().equals(nom) && c.getMdp().equals(mdp))
			{
				current_user.changer(c);
				return "Welcome " + current_user.getNom() + " !";
			}
		}
        return "Wrong password !";
    }

    /**
     * Permet d'afficher la liste des utilisateur.
     * @return la liste des utilisateurs si le credit de l'utilisateur courant est de 5 ou plus, sinon renvois une liste vide.
     */
	@RequestMapping("/listUser")
	public ArrayList<Utilisateur> afficherList() {
        if(current_user.testCredit())
			return User;
		else
			return NoUser;
    }
	
    /**
     * Permet de deconnecter l'utilisateur.
     * @return un message en cas de reussite ("Vous etes deconnecte.") ou un message d'erreur si l'utilisateur courant est vide.
     */
	@RequestMapping("/logout")
	public String logout() {
		if(current_user.getNom().equals("")){
			return "Vous n'etes actuellement pas connecte";
		}
		current_user.effacer();
		return "Vous etes deconnecte.";
	}
	
    /**
     * Permet de connecter l'utilisateur.
     * @param credit
     * 				le nombre de credit qui va etre confere a l'utilisateur courant.
     * @return un message en cas de reussite ("You are powerfull") ou un message d'erreur si la valeur entre n'etait pas attendu (if).
     */
	@RequestMapping("/give")
	public String give(@RequestParam(value="credit", required=true) int credit) {
		if(credit < 0 || credit > 5){
			return "Vous devez mettre une valeur comprise entre 0 et 5";
		}
		current_user.switchCredit(credit);
		for(Utilisateur c : User)
        {
			if(c.getNom().equals(current_user.getNom())){
				c.switchCredit(credit);
			}
		}
		return "You are powerfull";
	}


/**
 * Utilise la methode @see init pour s'assurer que la liste des taches n'est pas vide, puis renvoit la liste des taches grace a la requete GET.
 * 
 * @return la liste des taches actuelles de l'application.
 */
    @RequestMapping( path="/",method = RequestMethod.GET)
    public ArrayList<Task> index() {
    	init();
        return tasks;
    }
/**
 * Permet d'ajouter une tache a la liste des taches.
 * @param ajout
 * 				le nom de l'objet a ajouter, si aucun nom est indique, prend la valeur "nothing".
 * @param model
 * 				WIP
 * @return la liste des taches avec l'element ajoute.
 */

    @RequestMapping("/ajout")
    public ArrayList<Task> ajout(@RequestParam(value="ajout", required=false, defaultValue="nothing") String ajout, Model model) {
        model.addAttribute("ajout", ajout);
        tasks.add(new Task("tache : " + ajout + "  de " + current_user.getNom() + " " + current_user.getPrenom()));
        return tasks;
    }
    /**
     * Vide completement la liste des taches.
     * @return la liste des taches, vide.
     */
    @RequestMapping("/clear")
    public ArrayList<Task> clear() {
        tasks.clear();
        return tasks;
    }
    /**
     * Permet de retirer un objet de la liste en utilisant son identifiant.
     * @param delete 
     * 				L'identifiant de l'objet a supprimer.
     * @param model
     * 				WIP
     * @return la nouvelle liste des taches avec l'element supprime.
     */
    @RequestMapping("/delete")
    public ArrayList<Task> delete(@RequestParam(value="delete", required=false, defaultValue="-1") int delete, Model model) {
        model.addAttribute("delete", delete);
        tasks.remove(delete);
        return tasks;
    }
/**
 * Permet a l'utilisateur de choisir un nom de tache a ajouter via la requete POST.
 * @param title 
 * 				le nom de la tache a ajouter.
 */
	@RequestMapping( path="/",method = RequestMethod.POST)
     public void ajout(@RequestBody String title) {
     	tasks.add(new Task(title));
	}
	/**
	 * Recupere toute entree non prevue et affiche un message d'erreur sur la page.
	 * @return le message d'erreur "fallback method"
	 */
	@RequestMapping("*")
	public String fallbackMethod(){
	return "fallback method";
	}
}
