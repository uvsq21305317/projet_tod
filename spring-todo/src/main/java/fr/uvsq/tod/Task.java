/**
 * Task represente une tache que l'ont peut creer et manipuler à l'aide des requetes.
 * 
 * @author Romain
 * @version 1.0
 * 
 * 18/11/2017
 * */

package fr.uvsq.tod;

public class Task {

	/**
	 * title represente le nom de la tache.
	 */
    private final String title;
    
    /**
     * Constructeur pour une instance de "Task".
     * @param title nom de la tache
     */
    public Task(String title) {
        this.title = title;
    }
    /**
     * "getter" pour recuperer le nom de la tache courante.
     * @return le nom de la tache
     */
    public String getTitle() {
        return this.title;
    }
}