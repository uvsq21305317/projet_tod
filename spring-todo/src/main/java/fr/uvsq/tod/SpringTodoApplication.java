package fr.uvsq.tod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
/**
 * SpringTodoApplication possede le main() du programme et permet d'envoyer une requete a l'application
 * 
 * @author Romain
 *
 * @version 1.0
 * 
 * 18/11/2017
 * 
 * */
@SpringBootApplication
public class SpringTodoApplication {
	/**
	 * Permet a l'utilisateur d'interagir avec l'application
	 * 
	 * @param args
	 * 				la requete envoyee à l'application
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringTodoApplication.class, args);
	}
}
