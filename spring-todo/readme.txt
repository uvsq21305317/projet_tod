commandes:mvn package && java -jar target/spring-todo-0.0.1-SNAPSHOT.jar
	curl localhost:8080/
	go to /projet_tod/spring-todo/src/main/resources/static/index.html to open home page



liste des commandes pour simuler une intéraction :

http://localhost:8080/addUser?nom=Bernard&prenom=Jean&mdp=azerty
Bernard Jean

http://localhost:8080/addUser?nom=Lucie&prenom=stela&mdp=qwerty
Lucie stela

http://localhost:8080/login?nom=Lucie&mdp=qwerty
Welcome stela !

http://localhost:8080/login?nom=Lucie&mdp=qwerty
Vous êtes déjà connecté Lucie.

http://localhost:8080/ajout?ajout=test
[{"title":"Tâche : test stela Lucie"}]

http://localhost:8080/login?nom=Bernard&mdp=azerty
Welcome Jean !

http://localhost:8080/ajout?ajout=test
[{"title":"Tâche : test stela Lucie"},{"title":"testJean Bernard"}]

http://localhost:8080/listUser
[]

http://localhost:8080/give?credit=5
You are powerfull

http://localhost:8080/listUser
[{"prenom":"Lucie","nom":"stela","mdp":"qwerty","credit":0},{"prenom":"Bernard","nom":"Jean","mdp":"azerty","credit":5}]

